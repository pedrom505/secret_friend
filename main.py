import smtplib
import json
import random
from email.mime.text import MIMEText

def main():
    friend_list = get_list_of_participants("list.json")
    secret_result = select_secret_friend(friend_list)

    for secret in secret_result:
        print(f'pessoa: {secret[0]["nome"]} / amigo: {secret[1]["nome"]}')
        send_draw_result(secret[0]["nome"], secret[1]["nome"], secret[0]["email"])


def get_list_of_participants(file_path: str):
    with open(file_path) as json_file:
        data = json.load(json_file)
    return data['list']


def select_secret_friend(friend_list: list):
    participant_list = friend_list.copy()
    secret_friend_list = friend_list.copy()

    draw_result = []
    for participant in participant_list:
        
        secret_friend = participant
        while participant == secret_friend:
            index = random.randint(0, len(secret_friend_list)-1)
            secret_friend = secret_friend_list[index]
        secret_friend_list.pop(index)

        draw_result.append((participant, secret_friend))

    return draw_result

def send_draw_result(person, friend, email):
    smtp_ssl_host = 'smtp.gmail.com'
    smtp_ssl_port = 465

    username = 'amigosecretoanhaia@gmail.com'
    password = 'anhaia123'

    from_addr = 'amigosecretoanhaia@gmail.com'
    to_addrs = [email]

    message = MIMEText('Dae '+person +','+
                        '\n\n Seu amigo(a) secreto é o(a): '+friend +
                        ' \n\n E olhe lá, sem zueiras hein hahaha'+
                        ' \n\n O valor do presente é R$ 30,00' +
                        '\n\n Até dia 24/12 meu povo...')

    message['subject'] = 'TESTE - '+person+': Descubra quem é o seu amigo secreto'
    message['from'] = from_addr
    message['to'] = ', '.join(to_addrs)

    server = smtplib.SMTP_SSL(smtp_ssl_host, smtp_ssl_port)
    server.login(username, password)
    server.sendmail(from_addr, to_addrs, message.as_string())
    server.quit()


if __name__ == "__main__":
    main()




